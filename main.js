const smile = document.querySelectorAll('.sml');

// by index
// smile.forEach((el, i) => {
//     let counter = 1;
//     el.addEventListener('click', () => {
//         smile[i].nextElementSibling.innerText = counter++;
//     });
// });

// by e.target
smile.forEach(el => {
    let counter = 1;
    el.addEventListener('click', (e) => {
        e.target.nextElementSibling.innerText = counter++;
    });
});